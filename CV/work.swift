//
//  work.swift
//  CV
//
//  Created by Voltair on 2019-11-11.
//  Copyright © 2019 Albert. All rights reserved.
//

import Foundation
import UIKit

class work {
    
    private static let work_id = "work_id"
    var id: Int
    var name: String
    var  image: String
    var time: String
    var info: String
    
    init(_ id: Int,_ name: String,_ image: String,_ time: String,_ info: String=""){
        self.id = id
        self.name = name
        self.image = image
        self.time = time
        self.info = info
    }
    
    static func getWorkId() -> Int {
        // UserDefaults is a key-value storage which means that it basically is a Dictionary that looks like this: [String: Any] i.e. [Key: Value]
        return UserDefaults.standard.integer(forKey: work_id)
    }
    
    static func setWorkId(_ id: Int) {
        UserDefaults.standard.set(id, forKey: work_id)
    }
    
 
    
}
