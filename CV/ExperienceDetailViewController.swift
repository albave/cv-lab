//
//  ExperienceDetailViewController.swift
//  CV
//
//  Created by Voltair on 2019-11-11.
//  Copyright © 2019 Albert. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    @IBOutlet weak var workImage: UIImageView!
    @IBOutlet weak var workTitle: UILabel!
    @IBOutlet weak var workPeriod: UILabel!
    @IBOutlet weak var workDetails: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        workImage.image = UIImage(named: DataHandler.instance.getSelectedWork()?.image ?? "contract")
        workTitle.text = DataHandler.instance.getSelectedWork()?.name
        workPeriod.text = DataHandler.instance.getSelectedWork()?.time
        workDetails.text = DataHandler.instance.getSelectedWork()?.info
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
