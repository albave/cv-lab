//
//  ExperienceViewController.swift
//  CV
//
//  Created by Voltair on 2019-11-11.
//  Copyright © 2019 Albert. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {
    
    @IBOutlet weak var ExperianceTableView: UITableView!

    @IBOutlet weak var ExperianceTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ExperianceTableView.delegate = self
        ExperianceTableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return DataHandler.instance.works.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        if section == 0 {
            label.text = "Work"
        }
        else {
            label.text = "Education"
        }
        label.backgroundColor = UIColor.lightGray
        return label
    }
    
}

extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataHandler.instance.works[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "workCell", for: indexPath) as? TableViewCell {
            
            let work = DataHandler.instance.works[indexPath.section][indexPath.row]
            cell.ExperianceImage.image = UIImage(named: work.image)
            cell.ExperianceName.text = work.name
            cell.ExperiancePeriod.text = work.time
            return cell
            
        }
        return UITableViewCell()
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DataHandler.instance.setSelectedWork(withSection: indexPath.section ,withIndex: indexPath.row)
        
        
        // If you want to remove the selection color of the cell after tapping it you can simply do this:
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
    }
    
    
    

    
}



