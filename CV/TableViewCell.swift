//
//  TableViewCell.swift
//  CV
//
//  Created by Voltair on 2019-11-11.
//  Copyright © 2019 Albert. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var ExperianceImage: UIImageView!
    @IBOutlet weak var ExperianceName: UILabel!
    @IBOutlet weak var ExperiancePeriod: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
