//
//  SkillsViewController.swift
//  CV
//
//  Created by Voltair on 2019-11-27.
//  Copyright © 2019 Albert. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {


    @IBOutlet weak var yConstraints: NSLayoutConstraint!
    @IBOutlet weak var animationView: UIView!
    @IBAction func Exit(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.advancedAnimation()
        
        
    }
    
    func advancedAnimation(){
        
        UIView.animateKeyframes(withDuration: 2, delay: 0, options: .repeat, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1) {
                self.animationView.backgroundColor = UIColor.red
            }
           
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1) {
                self.animationView.transform = CGAffineTransform(translationX: 50, y: 40)
            }
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1) {
                self.animationView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5 )
            }
            UIView.addKeyframe(withRelativeStartTime: 1, relativeDuration: 1) {
                           self.animationView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                       }
        }) { (completed) in
            print("completed: \(completed)")
        }
        
    }
}
